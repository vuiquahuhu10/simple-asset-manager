# Simple Asset Manager

Simple Asset Manager for Blender 2.8. Works with **objects**, **materials**, **hdr** and **particle systems**.
Did this one due to dealys for internal asset manager for Blender.

**Made for simplicity of usage.**

- Opens `blend`, `obj`, `fbx`, `hdr`, `exr` from the same place.
- Dynamic, and custom categories: Except of `material`, and `particle` this addon will follow your library layout, folders and subfolders.
- creates structured collections: `Assets`, `Assets/Particles`
- With `Ctrl+scroll` over the categories it is even faster.
- Small and simple UI. Simple, and small to modify code as you like.
- option to batch render all previews.
- works as operator too (One can search `Simple Asset Manager` in command search, and assign a SHORTCUT!! :) But there is no default shortcut assigned for it)

None of the mangers I've tried was enough for me. Asset flinger was nice, but the idea with previews was much better. Chocofur asset manager is almost perfect, but still, it had its cons: separate spaces for categories, and (i only assume) fixed categories.

Focused on simplicity. Less buttons more functionality.

## Install

[Download this link SimpleAssetManager.zip file](https://gitlab.com/tibicen/simple-asset-manager/raw/master/SimpleAssetManager.zip)

Install .zip file without unpacking. Choose Library folder with your blends.

## Usage

After install you can also mark to render missing previews. It will render material view with eevee of your models, materials, and particle systems in one specific manner. Of course you can always render your own previews as 200x200 .png with the same name as blend file. But remember it can take awhile.

Manager tab is located in `Viewport tool panel` (shortcut key: `N`).
You will see all your folders as categories, and if any folder has additional folders you will see them as subcategory. For simplicity i didn't add any additinal ones.

**For simple usage i made some hacks.** So in folder named `materials` the addon will only import materials, and if the folder is called `particles`, then it will import all the particle systems. All the rest of the folders will import you blend, obj, fbx models, and hdr images.


![Small and handy.](preview.gif "Small and handy")


## TODO:

- (!) search option!;
- (?) add licence info (would be nice to add separate licence parameter for each object in file, then with one click can generate credits for everyone).
